/* Copyright 2021 @ Mike Killewald
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include QMK_KEYBOARD_H
#include "command.h"
#include "keymap_user.h"
#include "rgb_matrix.h"
#include "rgb_matrix_user.h"

#ifdef SNAKE_ENABLE
#    include "snake.h"
#endif

static bool is_not_transparent(uint16_t keycode) {
    return keycode != KC_TRNS && keycode != KC_NO && keycode != XXXXXXX;
}
static bool is_caps_lock_indicator(uint16_t keycode) {
    return keycode == KC_CAPS;
}

static keypos_t led_index_to_key_position[RGB_MATRIX_LED_COUNT];

/// setups led_index_to_key_position
void rgb_matrix_init_user(void) {
    for (uint8_t row = 0; row < MATRIX_ROWS; row++) {
        for (uint8_t col = 0; col < MATRIX_COLS; col++) {
            uint8_t led_index = g_led_config.matrix_co[row][col];
            if (led_index != NO_LED) {
                led_index_to_key_position[led_index] = (keypos_t){.row = row, .col = col};
                rgb_matrix_set_color(led_index, 255, 255, 255);
            }
        }
    }
}

/// Set leds colors that matches predicate is_keycode
void rgb_matrix_set_color_by_keycode(uint8_t led_min, uint8_t led_max, uint8_t layer, bool (*is_keycode)(uint16_t), uint8_t red, uint8_t green, uint8_t blue) {
    for (uint8_t i = led_min; i < led_max; i++) {
        uint16_t keycode = keymap_key_to_keycode(layer, led_index_to_key_position[i]);
        if ((*is_keycode)(keycode)) {
            rgb_matrix_set_color(i, red, green, blue);
        }
    }
}

static bool    first_seconds     = true;
static uint8_t first_seconds_cnt = 0;

static HSV effect_startup(HSV hsv, uint8_t i, uint8_t time) {
    // based on RAINBOW_MOVING_CHEVRON_math
    hsv.h += abs(g_led_config.point[i].y - 3) + (g_led_config.point[i].x - time);
    return hsv;
}

/// Called every cycle to override led colors
bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    if (first_seconds) {
        first_seconds_cnt++;
        if (first_seconds_cnt == 90)
            first_seconds = false;
        else {
            HSV   hsv = {0, 255, 255};
            float val = first_seconds_cnt / 128.0;
            hsv.v     = 255 * (1.0 - val * val * val);
            for (uint8_t i = led_min; i < led_max; i++) {
                RGB rgb = hsv_to_rgb(effect_startup(hsv, i, first_seconds_cnt * 2));
                rgb_matrix_set_color(i, rgb.r, rgb.g, rgb.b);
            }
        }
    }

    extern const uint8_t layers_colors[][3];

    uint8_t        current_layer = get_highest_layer(layer_state);
    const uint8_t* color         = layers_colors[current_layer];

    // Only override colors if the layer is colored
    if (color[0] || color[1] || color[2]) {
        rgb_matrix_set_color_by_keycode(led_min, led_max, current_layer, is_not_transparent, color[0], color[1], color[2]);
    }

#ifdef CAPS_LOCK_INDICATOR_COLOR
    // Set caps lock key color
    if (current_layer == MAC_BASE || current_layer == WIN_BASE) {
        if (host_keyboard_led_state().caps_lock) {
            rgb_matrix_set_color_by_keycode(led_min, led_max, current_layer, is_caps_lock_indicator, CAPS_LOCK_INDICATOR_COLOR);
        }
    }
#endif

#ifdef SNAKE_ENABLE
    if (current_layer == SNAKE) snake_paint_rgb_matrix();
#endif

    return false;
}
