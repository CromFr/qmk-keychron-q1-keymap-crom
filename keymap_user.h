#pragma once

// clang-format off
enum layers {
    MAC_BASE,
    MAC_FN,
    WIN_BASE,
    WIN_FN,
    KBSYS,
#ifdef SNAKE_ENABLE
    SNAKE
#endif
};

enum custom_keycodes {
    UC_TFLIP = SAFE_RANGE,
    UC_TUNFL,
    UC_TFLFL,
    UC_SHRG,
    UC_LENNY,
    UC_LENNYTIR,
#ifdef SNAKE_ENABLE
    KC_SNAKE_RIGHT,
    KC_SNAKE_DOWN,
    KC_SNAKE_LEFT,
    KC_SNAKE_UP,
    KC_SNAKE_RESTART,
#endif
#ifdef SOCD_ENABLE
    SOCDON,
    SOCDOFF,
    SOCDTOG,
#endif
};

#ifdef SNAKE_ENABLE
#define KC_SNAKE TG(SNAKE)
#else
#define KC_SNAKE KC_NO
#endif
// clang-format on
