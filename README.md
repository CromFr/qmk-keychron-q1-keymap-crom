# How to

1. Clone QMK Firmware: `git clone https://github.com/qmk/qmk_firmware.git --recurse`
2. Clone this repo
	- `git clone git@gitlab.com:CromFr/qmk-keychron-q1-keymap-crom.git qmk_firmware/keyboards/keychron/q1v1/iso/keymaps/crom`
	- `git clone https://gitlab.com/CromFr/qmk-keychron-q1-keymap-crom.git qmk_firmware/keyboards/keychron/q1v1/iso/keymaps/crom`
3. Compile: `cd qmk_firmware/keyboards/keychron/q1v1/iso/keymaps/crom; qmk compile -j24`
3. Flash: `cd qmk_firmware/keyboards/keychron/q1v1/iso/keymaps/crom; qmk flash`

# Troubleshooting

"Chip already blank, to force erase use --force" => `dfu-programmer atmega32u4 erase --force`