#include QMK_KEYBOARD_H
#include "snake.h"
#include "keymap_user.h"

enum { SNAKE_DIR_RIGHT, SNAKE_DIR_DOWN, SNAKE_DIR_LEFT, SNAKE_DIR_UP };

static bool    init = false;
static uint8_t timer;
static uint8_t snake_blocks[SNAKE_MAX_LENGTH][2];
static uint8_t snake_blocks_ptr;
static uint8_t snake_blocks_size = 0;
static uint8_t snake_dir;

static uint8_t food_pos[2];
static bool    loose_state;

static void paint_scene(void) {
    for (size_t i = 0; i < snake_blocks_size; ++i) {
        uint8_t* pos       = snake_blocks[(snake_blocks_ptr + i) % SNAKE_MAX_LENGTH];
        uint8_t  led_index = g_led_config.matrix_co[pos[1]][pos[0]];

        if (i + 1 < snake_blocks_size)
            rgb_matrix_set_color(led_index, 0, 255, 0);
        else
            rgb_matrix_set_color(led_index, 0, 255, 64);
    }

    uint8_t led_index = g_led_config.matrix_co[food_pos[1]][food_pos[0]];
    rgb_matrix_set_color(led_index, 255, 255, 0);
}

static void place_food(void) {
    bool conflict;
    do {
        food_pos[0] = SNAKE_MIN_COL + rand() % (SNAKE_MAX_COL - SNAKE_MIN_COL + 1);
        food_pos[1] = SNAKE_MIN_ROW + rand() % (SNAKE_MAX_ROW - SNAKE_MIN_ROW + 1);

        conflict = false;
        for (size_t i = 0; i < snake_blocks_size; ++i) {
            uint8_t* pos = snake_blocks[(snake_blocks_ptr + i) % SNAKE_MAX_LENGTH];
            if (pos[0] == food_pos[0] && pos[1] == food_pos[1]) {
                conflict = true;
                break;
            }
        }
    } while (conflict);
}
static void snake_step(void) {
    if (loose_state) return;

    // Get next block
    uint8_t* head = snake_blocks[(snake_blocks_ptr + snake_blocks_size - 1) % SNAKE_MAX_LENGTH];
    int8_t   next_block[2];
    switch (snake_dir) {
        default:
        case SNAKE_DIR_RIGHT:
            next_block[0] = head[0] + 1;
            next_block[1] = head[1];
            break;
        case SNAKE_DIR_DOWN:
            next_block[0] = head[0];
            next_block[1] = head[1] + 1;
            break;
        case SNAKE_DIR_LEFT:
            next_block[0] = head[0] - 1;
            next_block[1] = head[1];
            break;
        case SNAKE_DIR_UP:
            next_block[0] = head[0];
            next_block[1] = head[1] - 1;
            break;
    }

    // Check out of bounds
    if (next_block[0] < SNAKE_MIN_COL || next_block[0] > SNAKE_MAX_COL || next_block[1] < SNAKE_MIN_ROW || next_block[1] > SNAKE_MAX_ROW) {
        loose_state = true;
        return;
    }
    // Check eat yourself
    for (size_t i = 0; i < snake_blocks_size; ++i) {
        uint8_t* pos = snake_blocks[(snake_blocks_ptr + i) % SNAKE_MAX_LENGTH];
        if (pos[0] == next_block[0] && pos[1] == next_block[1]) {
            loose_state = true;
            return;
        }
    }

    // Eat food
    if (next_block[0] == food_pos[0] && next_block[1] == food_pos[1] && snake_blocks_size < SNAKE_MAX_LENGTH) {
        snake_blocks[(snake_blocks_ptr + snake_blocks_size) % SNAKE_MAX_LENGTH][0] = food_pos[0];
        snake_blocks[(snake_blocks_ptr + snake_blocks_size) % SNAKE_MAX_LENGTH][1] = food_pos[1];
        snake_blocks_size++;

        place_food();
        return;
    }

    // Go forward
    snake_blocks[(snake_blocks_ptr + snake_blocks_size) % SNAKE_MAX_LENGTH][0] = next_block[0];
    snake_blocks[(snake_blocks_ptr + snake_blocks_size) % SNAKE_MAX_LENGTH][1] = next_block[1];

    snake_blocks_ptr = (snake_blocks_ptr + 1) % SNAKE_MAX_LENGTH;
}

void snake_init(void) {
    if (!init) {
        init = true;
        srand(TCNT0);
    }
    loose_state      = false;
    snake_blocks_ptr = 0;

    snake_blocks[snake_blocks_ptr][0] = SNAKE_MIN_COL + (SNAKE_MAX_COL - SNAKE_MIN_COL) / 2;
    snake_blocks[snake_blocks_ptr][1] = SNAKE_MIN_ROW + (SNAKE_MAX_ROW - SNAKE_MIN_ROW) / 2;
    snake_blocks_size                 = 1;

    snake_dir = SNAKE_DIR_RIGHT;

    place_food();
}

void snake_paint_rgb_matrix(void) {
    timer++;
    if (loose_state) {
        if (timer % 64 < 32) {
            paint_scene();
        }
        return;
    }

    if ((timer % 32) == 0) snake_step();
    paint_scene();
}

bool snake_process_record(uint16_t keycode, keyrecord_t* record) {
    if (!record->event.pressed) return false;

    switch (keycode) {
        case KC_SNAKE_RIGHT:
            if (snake_dir != SNAKE_DIR_LEFT) snake_dir = SNAKE_DIR_RIGHT;
            return false;
        case KC_SNAKE_DOWN:
            if (snake_dir != SNAKE_DIR_UP) snake_dir = SNAKE_DIR_DOWN;
            return false;
        case KC_SNAKE_LEFT:
            if (snake_dir != SNAKE_DIR_RIGHT) snake_dir = SNAKE_DIR_LEFT;
            return false;
        case KC_SNAKE_UP:
            if (snake_dir != SNAKE_DIR_DOWN) snake_dir = SNAKE_DIR_UP;
            return false;
        case KC_SNAKE_RESTART:
            snake_init();
            return false;
    }
    return true;
}