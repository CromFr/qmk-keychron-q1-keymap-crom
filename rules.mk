# ALLOW_WARNINGS=yes
VIA_ENABLE = no # Prevents from binding keys with builtin keymaps
MOUSEKEY_ENABLE = no
CONSOLE_ENABLE = no
SNAKE_ENABLE = yes
SOCD_ENABLE = yes
LTO_ENABLE = yes # Note: disables the legacy TMK Macros and Functions features
RGB_MATRIX_CUSTOM_USER = YES
PROGRAMMABLE_BUTTON_ENABLE = yes

ifeq ($(strip $(RGB_MATRIX_ENABLE)), yes)
    SRC += rgb_matrix_user.c
endif
ifeq ($(strip $(SNAKE_ENABLE)), yes)
    SRC += snake.c
    CDEFS += -DSNAKE_ENABLE
endif

UNICODE_ENABLE = yes
SRC += unicode_fr.c

ifeq ($(strip $(SOCD_ENABLE)), yes)
# SOCD / snap tap
# https://getreuer.info/posts/keyboards/socd-cleaner/
SRC += socd/socd_cleaner.c
CDEFS += -DSOCD_ENABLE
endif