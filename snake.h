#pragma once

#define SNAKE_MIN_ROW 1
#define SNAKE_MAX_ROW 4

#define SNAKE_MIN_COL 1
#define SNAKE_MAX_COL 11

#define SNAKE_MAX_LENGTH 20

/// Call this in keyboard_post_init_user
void snake_init(void);

/// Call this in rgb_matrix_indicators_advanced_user if the snake layer is activated
void snake_paint_rgb_matrix(void);

/// Call this in process_record_user
bool snake_process_record(uint16_t keycode, keyrecord_t *record);